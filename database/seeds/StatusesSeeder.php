<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class StatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
        [
            
            'name' => 'before interview',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],
        [
            
            'name' => 'not fit',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],
        [
            
            'name' => 'sent to manager',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],
        [
            
            'name' => 'not fit professionally',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],
        [
            
            'name' => 'accepted to work',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],
    ]);
    }
}

