@extends('layouts.app')

@section('content')
@section('title', 'Craete candidate')

        <h1>Create candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@store')}}">
        @csrf
        <div class="form-group">
            <lable for= "name">Candidate name</lable> 
            <input type = "text" class="form-control" name = "name" placeholder="Enter name">  
        </div> 
        <div class="form-group">
            <lable for= "email">Candidate email</lable> 
            <input type = "text" class="form-control" name = "email" placeholder="Enter email">  
        </div>
        <div>
            <input type= "submit" name= "submit" value= "Create candidate" class="btn btn-outline-dark"> 
        </div>
        </form>
@endsection

