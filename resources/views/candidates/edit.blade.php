@extends('layouts.app')

@section('content')
@section('title', 'Edit candidate')
        <h1>Edit candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@update',$candidate->id)}}">
        @method('PATCH')
        @csrf
        <div>
            <lable for = "name">Candidate name</lable> 
            <input type = "text" name= "name" value = {{$candidate->name}}>  
        </div> 
        <div>
            <lable for= "email">Candidate email</lable> 
            <input type= "text" name= "email" value = {{$candidate->email}}> 
        </div>
        <div>
            <input type= "submit" name= "submit" value= "Update candidate" class="btn btn-outline-dark">  
        </div>
        </form>
@endsection
